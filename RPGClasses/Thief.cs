﻿namespace RPGClasses
{
    public class Thief : Character
    {
        public int Stealth { get; set; }
        public Thief(int id, string name, int hp, int physAttack, int stealth) : base(id, name, hp, physAttack)
        {
            Stealth = stealth;
        }
        public override void Attack(Character enemy)
        {
            enemy.HP -= PhysAttack;
        }
        public override int GetUnique() => Stealth;
    }
}
