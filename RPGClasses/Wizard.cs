﻿namespace RPGClasses
{
    public class Wizard : Character
    {
        public int MagicAttack { get; set; }

        public Wizard(int id, string name, int hp, int physAttack, int magicAttack) : base(id, name, hp, physAttack)
        {
            MagicAttack = magicAttack;
        }
        public override void Attack(Character enemy)
        {
            enemy.HP -= MagicAttack;
        }
        public new string Summary()
        {
            return base.Summary() + "MAGIC ATTACK: " + MagicAttack + "\n";
        }
        public override int GetUnique() => MagicAttack;
    }
}
