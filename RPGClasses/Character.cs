﻿using System;
using System.Threading.Tasks;

namespace RPGClasses
{
    public abstract class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int HP { get; set; }
        public int PhysAttack { get; set; }

        public Character(int id, string name, int hp, int physAttack)
        {
            Id = id;
            Name = name;
            HP = hp;
            PhysAttack = physAttack;
        }
        public void Move(string direction, int meters)
        {
            Console.WriteLine(Name + " moved " + meters + " to the " + direction + ".");
        }
        public abstract void Attack(Character enemy);
        public string Summary()
        {
            return "NAME: " + Name
                + "\nHP: " + HP
                + "\nATTACK: " + PhysAttack + "\n";
        }
        public abstract int GetUnique();
    }
}
