﻿namespace RPGClasses
{
    public class Warrior : Character
    {
        public int Armor { get; set; }
        public Warrior(int id, string name, int hp, int physAttack, int armor) : base(id, name, hp, physAttack)
        {
            Armor = armor;
        }
        public override void Attack(Character enemy)
        {
            enemy.HP -= PhysAttack;
        }
        public override int GetUnique() => Armor;
    }
}
