CREATE TABLE Character (
	id		int		PRIMARY KEY IDENTITY(1,1),
	Class	nvarchar(50)	NOT NULL,
	Name	nvarchar(50)	NOT NULL,
	HP		int				NOT NULL,
	Attack	int			NOT NULL,
	UniqueAttribute	int		NOT NULL,
);

INSERT INTO Character (Class, Name, HP, Attack, UniqueAttribute) VALUES ('Thief', 'Fenton', 10, 5, 20);
INSERT INTO Character (Class, Name, HP, Attack, UniqueAttribute) VALUES ('Warrior', 'Ragnar', 20, 10, 5);
INSERT INTO Character (Class, Name, HP, Attack, UniqueAttribute) VALUES ('Wizard', 'Magnus', 5, 5, 30);