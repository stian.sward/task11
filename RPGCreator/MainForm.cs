﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGClasses;

namespace RPGCreator
{
    public partial class MainForm : Form
    {
        private BindingList<Character> charList;
        string uniqueAttributeName;
        SqlConnectionStringBuilder builder;
        public MainForm()
        {
            uniqueAttributeName = "";
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Set up database connection properties
            builder = new SqlConnectionStringBuilder();
            builder.DataSource = "STIAN-DESKTOP\\SQLEXPRESS";
            builder.InitialCatalog = "RPGDB";
            builder.IntegratedSecurity = true;

            // Populate class selector combobox
            cbClassSelect.Items.Add("Thief");
            cbClassSelect.Items.Add("Warrior");
            cbClassSelect.Items.Add("Wizard");

            // Create internal data structure from DB
            charList = LoadDB(builder);

            // Connect listBox to internal data structure
            lbCharSelect.DataSource = charList;
            lbCharSelect.DisplayMember = "Name";
        }

        //
        // Focus changes
        //
        private void tbName_Enter(object sender, EventArgs e)
        {
            if (tbName.Text == "NAME")
            {
                tbName.Text = "";
                tbName.ForeColor = Color.Black;
            }
        }

        private void tbName_Leave(object sender, EventArgs e)
        {
            if (tbName.Text == "")
            {
                tbName.ForeColor = Color.Gray;
                tbName.Text = "NAME";
            }
        }

        private void tbHP_Enter(object sender, EventArgs e)
        {
            if (tbHP.Text == "HP")
            {
                tbHP.Text = "";
                tbHP.ForeColor = Color.Black;
            }
        }

        private void tbHP_Leave(object sender, EventArgs e)
        {
            if (tbHP.Text == "")
            {
                tbHP.ForeColor = Color.Gray;
                tbHP.Text = "HP";
            }
        }

        private void tbAttack_Enter(object sender, EventArgs e)
        {
            if (tbAttack.Text == "ATTACK")
            {
                tbAttack.Text = "";
                tbAttack.ForeColor = Color.Black;
            }
        }

        private void tbAttack_Leave(object sender, EventArgs e)
        {
            if (tbAttack.Text == "")
            {
                tbAttack.ForeColor = Color.Gray;
                tbAttack.Text = "ATTACK";
            }
        }

        private void tbUnique_Enter(object sender, EventArgs e)
        {
            if (tbUnique.Text == uniqueAttributeName)
            {
                tbUnique.Text = "";
                tbUnique.ForeColor = Color.Black;
            }
        }

        private void tbUnique_Leave(object sender, EventArgs e)
        {
            if(tbUnique.Text == "")
            {
                tbUnique.ForeColor = Color.Gray;
                tbUnique.Text = uniqueAttributeName;
            }
        }

        //
        // Class selection
        //
        private void cbClassSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbUnique.Enabled = (cbClassSelect.SelectedIndex != -1);
            UpdateUniqueAttributeName((string)cbClassSelect.SelectedItem);
            tbUnique.Text = uniqueAttributeName;
        }

        //
        // Character creation
        //
        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                int id = 0;
                // Add character to database
                try
                {
                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        using (SqlCommand command = new SqlCommand("INSERT INTO Character (Class, Name, HP, PhysAttack, UniqueAttribute) VALUES (@Class, @Name, @HP, @PhysAttack, @Unique); SELECT SCOPE_IDENTITY()", connection))
                        {
                            command.Parameters.AddWithValue("@Class", cbClassSelect.SelectedItem);
                            command.Parameters.AddWithValue("@Name", tbName.Text);
                            command.Parameters.AddWithValue("@HP", tbHP.Text);
                            command.Parameters.AddWithValue("@PhysAttack", tbAttack.Text);
                            command.Parameters.AddWithValue("@Unique", tbUnique.Text);

                            id = Convert.ToInt32(command.ExecuteScalar());
                        }
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                // Add character to charList
                Type classType = Type.GetType("RPGClasses." + (string)cbClassSelect.SelectedItem 
                    + ", RPGClasses, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
                charList.Add((Character)Activator.CreateInstance(classType, id, tbName.Text, 
                    int.Parse(tbHP.Text), int.Parse(tbAttack.Text), int.Parse(tbUnique.Text)));
            }

            // Update GUI to show newly created character, and clear creation form
            lbCharSelect.Update();
            lbCharSelect.SetSelected(lbCharSelect.Items.Count - 1, true);
            tbName.ForeColor = Color.Gray;
            tbHP.ForeColor = Color.Gray;
            tbAttack.ForeColor = Color.Gray;
            tbUnique.ForeColor = Color.Gray;
            tbName.Text = "NAME";
            cbClassSelect.SelectedIndex = -1;
            cbClassSelect.Text = "Select class";
            tbHP.Text = "HP";
            tbAttack.Text = "ATTACK";
            tbUnique.Text = "";
        }

        private bool ValidateInput()
        {
            if (tbName.Text == "" || cbClassSelect.SelectedIndex == -1)
            {
                return false;
            }
            try
            {
                int.Parse(tbHP.Text);
                int.Parse(tbAttack.Text);
                int.Parse(tbUnique.Text);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        //
        // Update character
        //
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateForm window = new UpdateForm(charList, builder, (Character)lbCharSelect.SelectedItem, this);
            window.Show();
        }

        public void lbCharSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character character = (Character)lbCharSelect.SelectedItem;
            if (character != null)
            {
                UpdateUniqueAttributeName(character.GetType().Name);
                lblName.Text = character.Name;
                lblHP.Text = character.HP.ToString();
                lblAttack.Text = character.PhysAttack.ToString();
                lblUniqueLabel.Text = uniqueAttributeName + ":";
                lblUnique.Text = character.GetUnique().ToString();
            }
            else
            {
                UpdateUniqueAttributeName("");
                lblName.Text = "";
                lblHP.Text = "";
                lblAttack.Text = "";
                lblUniqueLabel.Text = "";
                lblUnique.Text = "";
            }
        }

        //
        // Utility methods
        //
        private void UpdateUniqueAttributeName(string type)
        {
            switch (type)
            {
                case "Thief":
                    uniqueAttributeName = "STEALTH";
                    break;
                case "Warrior":
                    uniqueAttributeName = "ARMOR";
                    break;
                case "Wizard":
                    uniqueAttributeName = "MAGIC ATTACK";
                    break;
                default:
                    uniqueAttributeName = "";
                    break;
            }
        }

        //
        // Data storage
        //

        /**
         * DEPRECATED
         *
        private void btnSave_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter("characters.csv"))
            {
                // Cannot use CsvHelper due to differing attribute names among classes
                // Creating custom .csv file instead
                string[] columns = { "Name", "HP", "PhysAttack", "Armor", "MagicAttack", "Stealth"};
                writer.WriteLine(String.Join(",",columns));
                string[] values = new string[columns.Length];
                foreach (Character character in charList)
                {
                    Array.Clear(values, 0, values.Length);
                    PropertyInfo[] properties = character.GetType().GetProperties();
                    foreach (PropertyInfo property in properties)
                    {
                        if (property.Name != "Id")
                        {
                            values[Array.IndexOf(columns, property.Name)] = property.GetValue(character).ToString();
                        }
                    }
                    writer.WriteLine(String.Join(",", values));
                }
            }
            MessageBox.Show("Characters saved!");
        }
        */



        private void btnDelete_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("DELETE FROM Character WHERE Id = @Id;", connection))
                {
                    Character character = (Character)lbCharSelect.SelectedItem;
                    command.Parameters.AddWithValue("@Id", character.Id);
                    command.ExecuteNonQuery();
                }
            }
            
            charList.Remove((Character)lbCharSelect.SelectedItem);
        }

        /** DEPRECATED
         *  Loads .CSV file into internal data structure
        private BindingList<Character> LoadCSV(string filename)
        {
            BindingList<Character> charList = new BindingList<Character>();
            using (StreamReader reader = new StreamReader("characters.csv"))
            {
                if (reader.Peek() > 0)
                {
                    string[] columns = reader.ReadLine().Split(',');
                    string[] values;
                    while (reader.Peek() >= 0)
                    {
                        values = reader.ReadLine().Split(',');
                        if (values[3] != "")
                        {
                            charList.Add(new Warrior(values[0], int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[3])));
                        }
                        else if (values[4] != "")
                        {
                            charList.Add(new Wizard(values[0], int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[4])));
                        }
                        else if (values[5] != "")
                        {
                            charList.Add(new Thief(values[0], int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[5])));
                        }
                    }
                }
            }
            return charList;
        }
        */

        private BindingList<Character> LoadDB(SqlConnectionStringBuilder builder)
        {
            BindingList<Character> charList = new BindingList<Character>();

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string query = "SELECT * FROM Character;";
                    Type classType = null;
                    string name;
                    int id, hp, attack, unique;

                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Create object of type specified by 'Class' column in database, add to charList
                                classType = Type.GetType("RPGClasses." + reader.GetString(1) + ", RPGClasses, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
                                id = reader.GetInt32(0);
                                name = reader.GetString(2);
                                hp = reader.GetInt32(3);
                                attack = reader.GetInt32(4);
                                unique = reader.GetInt32(5);
                                charList.Add((Character)Activator.CreateInstance(classType, id, name, hp, attack, unique));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return charList;
        }
    }
}
