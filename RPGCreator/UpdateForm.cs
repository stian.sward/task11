﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGClasses;

namespace RPGCreator
{
    public partial class UpdateForm : Form
    {
        private BindingList<Character> charList;
        string uniqueAttributeName;
        SqlConnectionStringBuilder builder;
        Character character;
        MainForm mainWindow;
        public UpdateForm()
        {
            InitializeComponent();
        }
        public UpdateForm(BindingList<Character> charList, SqlConnectionStringBuilder builder, Character character, MainForm mainWindow)
        {
            this.charList = charList;
            this.builder = builder;
            this.character = character;
            this.mainWindow = mainWindow;
            InitializeComponent();
        }

        private void UpdateForm_Load(object sender, EventArgs e)
        {
            // Populate class selector combobox
            cbClassSelect.Items.Add("Thief");
            cbClassSelect.Items.Add("Warrior");
            cbClassSelect.Items.Add("Wizard");

            cbClassSelect.SelectedItem = character.GetType().Name;
            UpdateUniqueAttributeName(character.GetType().Name);
            tbName.Text = character.Name;
            tbHP.Text = character.HP.ToString();
            tbAttack.Text = character.PhysAttack.ToString();
            lblUnique.Text = uniqueAttributeName + ":";
            tbUnique.Text = character.GetUnique().ToString();
        }

        //
        // Class selection
        //
        private void cbClassSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbUnique.Enabled = (cbClassSelect.SelectedIndex != -1);
            UpdateUniqueAttributeName((string)cbClassSelect.SelectedItem);
            lblUnique.Text = uniqueAttributeName + ":";
        }
        private void UpdateUniqueAttributeName(string type)
        {
            switch (type)
            {
                case "Thief":
                    uniqueAttributeName = "STEALTH";
                    break;
                case "Warrior":
                    uniqueAttributeName = "ARMOR";
                    break;
                case "Wizard":
                    uniqueAttributeName = "MAGIC ATTACK";
                    break;
                default:
                    uniqueAttributeName = "";
                    break;
            }
        }

        //
        // Character update
        //
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                // Update database
                try
                {
                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        using (SqlCommand command = new SqlCommand("UPDATE Character SET Class = @Class, Name = @Name, HP = @HP, PhysAttack = @PhysAttack, UniqueAttribute = @Unique WHERE Id = @ID", connection))
                        {
                            command.Parameters.AddWithValue("@Class", cbClassSelect.SelectedItem);
                            command.Parameters.AddWithValue("@Name", tbName.Text);
                            command.Parameters.AddWithValue("@HP", tbHP.Text);
                            command.Parameters.AddWithValue("@PhysAttack", tbAttack.Text);
                            command.Parameters.AddWithValue("@Unique", tbUnique.Text);
                            command.Parameters.AddWithValue("@ID", character.Id);

                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                // Update internal data structure
                charList.Remove(character);
                Type classType = Type.GetType("RPGClasses." + (string)cbClassSelect.SelectedItem
                    + ", RPGClasses, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
                charList.Add((Character)Activator.CreateInstance(classType, character.Id, tbName.Text,
                    int.Parse(tbHP.Text), int.Parse(tbAttack.Text), int.Parse(tbUnique.Text)));

                // Close update window
                mainWindow.Focus();
                mainWindow.lbCharSelect_SelectedIndexChanged(this, e);
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid input");
            }
        }

        private bool ValidateInput()
        {
            if (tbName.Text == "" || cbClassSelect.SelectedIndex == -1)
            {
                return false;
            }
            try
            {
                int.Parse(tbHP.Text);
                int.Parse(tbAttack.Text);
                int.Parse(tbUnique.Text);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
