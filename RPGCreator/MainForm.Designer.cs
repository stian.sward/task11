﻿using System;

namespace RPGCreator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCharCreate = new System.Windows.Forms.Label();
            this.lblCharList = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.cbClassSelect = new System.Windows.Forms.ComboBox();
            this.tbHP = new System.Windows.Forms.TextBox();
            this.tbAttack = new System.Windows.Forms.TextBox();
            this.tbUnique = new System.Windows.Forms.TextBox();
            this.lbCharSelect = new System.Windows.Forms.ListBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.gbSelected = new System.Windows.Forms.GroupBox();
            this.lblUnique = new System.Windows.Forms.Label();
            this.lblUniqueLabel = new System.Windows.Forms.Label();
            this.lblAttack = new System.Windows.Forms.Label();
            this.lblAttackLabel = new System.Windows.Forms.Label();
            this.lblHP = new System.Windows.Forms.Label();
            this.lblHPLabel = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblNameLabel = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.gbSelected.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCharCreate
            // 
            this.lblCharCreate.AutoSize = true;
            this.lblCharCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharCreate.Location = new System.Drawing.Point(12, 9);
            this.lblCharCreate.Name = "lblCharCreate";
            this.lblCharCreate.Size = new System.Drawing.Size(238, 25);
            this.lblCharCreate.TabIndex = 0;
            this.lblCharCreate.Text = "CHARACTER CREATOR";
            // 
            // lblCharList
            // 
            this.lblCharList.AutoSize = true;
            this.lblCharList.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharList.Location = new System.Drawing.Point(295, 9);
            this.lblCharList.Name = "lblCharList";
            this.lblCharList.Size = new System.Drawing.Size(150, 25);
            this.lblCharList.TabIndex = 1;
            this.lblCharList.Text = "CHARACTERS";
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName.ForeColor = System.Drawing.Color.Gray;
            this.tbName.Location = new System.Drawing.Point(17, 38);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(233, 26);
            this.tbName.TabIndex = 2;
            this.tbName.Text = "NAME";
            this.tbName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbName.Enter += new System.EventHandler(this.tbName_Enter);
            this.tbName.Leave += new System.EventHandler(this.tbName_Leave);
            // 
            // cbClassSelect
            // 
            this.cbClassSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClassSelect.FormattingEnabled = true;
            this.cbClassSelect.Location = new System.Drawing.Point(17, 71);
            this.cbClassSelect.Name = "cbClassSelect";
            this.cbClassSelect.Size = new System.Drawing.Size(233, 28);
            this.cbClassSelect.TabIndex = 3;
            this.cbClassSelect.Text = "Select class";
            this.cbClassSelect.SelectedIndexChanged += new System.EventHandler(this.cbClassSelect_SelectedIndexChanged);
            // 
            // tbHP
            // 
            this.tbHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHP.ForeColor = System.Drawing.Color.Gray;
            this.tbHP.Location = new System.Drawing.Point(17, 105);
            this.tbHP.Name = "tbHP";
            this.tbHP.Size = new System.Drawing.Size(233, 26);
            this.tbHP.TabIndex = 4;
            this.tbHP.Text = "HP";
            this.tbHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbHP.Enter += new System.EventHandler(this.tbHP_Enter);
            this.tbHP.Leave += new System.EventHandler(this.tbHP_Leave);
            // 
            // tbAttack
            // 
            this.tbAttack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAttack.ForeColor = System.Drawing.Color.Gray;
            this.tbAttack.Location = new System.Drawing.Point(17, 137);
            this.tbAttack.Name = "tbAttack";
            this.tbAttack.Size = new System.Drawing.Size(233, 26);
            this.tbAttack.TabIndex = 5;
            this.tbAttack.Text = "ATTACK";
            this.tbAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbAttack.Enter += new System.EventHandler(this.tbAttack_Enter);
            this.tbAttack.Leave += new System.EventHandler(this.tbAttack_Leave);
            // 
            // tbUnique
            // 
            this.tbUnique.Enabled = false;
            this.tbUnique.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUnique.ForeColor = System.Drawing.Color.Gray;
            this.tbUnique.Location = new System.Drawing.Point(17, 169);
            this.tbUnique.Name = "tbUnique";
            this.tbUnique.Size = new System.Drawing.Size(233, 26);
            this.tbUnique.TabIndex = 6;
            this.tbUnique.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbUnique.Enter += new System.EventHandler(this.tbUnique_Enter);
            this.tbUnique.Leave += new System.EventHandler(this.tbUnique_Leave);
            // 
            // lbCharSelect
            // 
            this.lbCharSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCharSelect.FormattingEnabled = true;
            this.lbCharSelect.ItemHeight = 18;
            this.lbCharSelect.Location = new System.Drawing.Point(257, 38);
            this.lbCharSelect.Name = "lbCharSelect";
            this.lbCharSelect.Size = new System.Drawing.Size(224, 202);
            this.lbCharSelect.TabIndex = 7;
            this.lbCharSelect.SelectedIndexChanged += new System.EventHandler(this.lbCharSelect_SelectedIndexChanged);
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.Location = new System.Drawing.Point(17, 202);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(233, 38);
            this.btnCreate.TabIndex = 8;
            this.btnCreate.Text = "CREATE CHARACTER";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // gbSelected
            // 
            this.gbSelected.Controls.Add(this.lblUnique);
            this.gbSelected.Controls.Add(this.lblUniqueLabel);
            this.gbSelected.Controls.Add(this.lblAttack);
            this.gbSelected.Controls.Add(this.lblAttackLabel);
            this.gbSelected.Controls.Add(this.lblHP);
            this.gbSelected.Controls.Add(this.lblHPLabel);
            this.gbSelected.Controls.Add(this.lblName);
            this.gbSelected.Controls.Add(this.lblNameLabel);
            this.gbSelected.Location = new System.Drawing.Point(17, 247);
            this.gbSelected.Name = "gbSelected";
            this.gbSelected.Size = new System.Drawing.Size(350, 103);
            this.gbSelected.TabIndex = 9;
            this.gbSelected.TabStop = false;
            this.gbSelected.Text = "SELECTED CHARACTER";
            // 
            // lblUnique
            // 
            this.lblUnique.AutoSize = true;
            this.lblUnique.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnique.Location = new System.Drawing.Point(173, 74);
            this.lblUnique.Name = "lblUnique";
            this.lblUnique.Size = new System.Drawing.Size(0, 18);
            this.lblUnique.TabIndex = 7;
            // 
            // lblUniqueLabel
            // 
            this.lblUniqueLabel.AutoSize = true;
            this.lblUniqueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUniqueLabel.Location = new System.Drawing.Point(7, 74);
            this.lblUniqueLabel.Name = "lblUniqueLabel";
            this.lblUniqueLabel.Size = new System.Drawing.Size(70, 18);
            this.lblUniqueLabel.TabIndex = 6;
            this.lblUniqueLabel.Text = "UNIQUE:";
            // 
            // lblAttack
            // 
            this.lblAttack.AutoSize = true;
            this.lblAttack.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack.Location = new System.Drawing.Point(173, 56);
            this.lblAttack.Name = "lblAttack";
            this.lblAttack.Size = new System.Drawing.Size(0, 18);
            this.lblAttack.TabIndex = 5;
            // 
            // lblAttackLabel
            // 
            this.lblAttackLabel.AutoSize = true;
            this.lblAttackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttackLabel.Location = new System.Drawing.Point(7, 56);
            this.lblAttackLabel.Name = "lblAttackLabel";
            this.lblAttackLabel.Size = new System.Drawing.Size(69, 18);
            this.lblAttackLabel.TabIndex = 4;
            this.lblAttackLabel.Text = "ATTACK:";
            // 
            // lblHP
            // 
            this.lblHP.AutoSize = true;
            this.lblHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP.Location = new System.Drawing.Point(173, 38);
            this.lblHP.Name = "lblHP";
            this.lblHP.Size = new System.Drawing.Size(0, 18);
            this.lblHP.TabIndex = 3;
            // 
            // lblHPLabel
            // 
            this.lblHPLabel.AutoSize = true;
            this.lblHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHPLabel.Location = new System.Drawing.Point(7, 38);
            this.lblHPLabel.Name = "lblHPLabel";
            this.lblHPLabel.Size = new System.Drawing.Size(33, 18);
            this.lblHPLabel.TabIndex = 2;
            this.lblHPLabel.Text = "HP:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(173, 20);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 18);
            this.lblName.TabIndex = 1;
            // 
            // lblNameLabel
            // 
            this.lblNameLabel.AutoSize = true;
            this.lblNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameLabel.Location = new System.Drawing.Point(7, 20);
            this.lblNameLabel.Name = "lblNameLabel";
            this.lblNameLabel.Size = new System.Drawing.Size(55, 18);
            this.lblNameLabel.TabIndex = 0;
            this.lblNameLabel.Text = "NAME:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(374, 247);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(107, 45);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "UPDATE";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(374, 299);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(107, 51);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 362);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.gbSelected);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lbCharSelect);
            this.Controls.Add(this.tbUnique);
            this.Controls.Add(this.tbAttack);
            this.Controls.Add(this.tbHP);
            this.Controls.Add(this.cbClassSelect);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblCharList);
            this.Controls.Add(this.lblCharCreate);
            this.Name = "Form1";
            this.Text = "RPG Character Creator";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.gbSelected.ResumeLayout(false);
            this.gbSelected.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCharCreate;
        private System.Windows.Forms.Label lblCharList;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.ComboBox cbClassSelect;
        private System.Windows.Forms.TextBox tbHP;
        private System.Windows.Forms.TextBox tbAttack;
        private System.Windows.Forms.TextBox tbUnique;
        private System.Windows.Forms.ListBox lbCharSelect;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.GroupBox gbSelected;
        private System.Windows.Forms.Label lblUnique;
        private System.Windows.Forms.Label lblUniqueLabel;
        private System.Windows.Forms.Label lblAttack;
        private System.Windows.Forms.Label lblAttackLabel;
        private System.Windows.Forms.Label lblHP;
        private System.Windows.Forms.Label lblHPLabel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblNameLabel;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
    }
}

