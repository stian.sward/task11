﻿namespace RPGCreator
{
    partial class UpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdate = new System.Windows.Forms.Button();
            this.tbUnique = new System.Windows.Forms.TextBox();
            this.tbAttack = new System.Windows.Forms.TextBox();
            this.tbHP = new System.Windows.Forms.TextBox();
            this.cbClassSelect = new System.Windows.Forms.ComboBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblHP = new System.Windows.Forms.Label();
            this.lblAttack = new System.Windows.Forms.Label();
            this.lblUnique = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(12, 176);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(233, 38);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "UPDATE CHARACTER";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tbUnique
            // 
            this.tbUnique.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUnique.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbUnique.Location = new System.Drawing.Point(141, 143);
            this.tbUnique.Name = "tbUnique";
            this.tbUnique.Size = new System.Drawing.Size(104, 26);
            this.tbUnique.TabIndex = 13;
            this.tbUnique.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbAttack
            // 
            this.tbAttack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAttack.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbAttack.Location = new System.Drawing.Point(141, 111);
            this.tbAttack.Name = "tbAttack";
            this.tbAttack.Size = new System.Drawing.Size(104, 26);
            this.tbAttack.TabIndex = 12;
            this.tbAttack.Text = "ATTACK";
            this.tbAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbHP
            // 
            this.tbHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHP.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbHP.Location = new System.Drawing.Point(141, 79);
            this.tbHP.Name = "tbHP";
            this.tbHP.Size = new System.Drawing.Size(104, 26);
            this.tbHP.TabIndex = 11;
            this.tbHP.Text = "HP";
            this.tbHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbClassSelect
            // 
            this.cbClassSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClassSelect.FormattingEnabled = true;
            this.cbClassSelect.Location = new System.Drawing.Point(12, 12);
            this.cbClassSelect.Name = "cbClassSelect";
            this.cbClassSelect.Size = new System.Drawing.Size(233, 28);
            this.cbClassSelect.TabIndex = 10;
            this.cbClassSelect.Text = "Select class";
            this.cbClassSelect.SelectedIndexChanged += new System.EventHandler(this.cbClassSelect_SelectedIndexChanged);
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbName.Location = new System.Drawing.Point(141, 47);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(104, 26);
            this.tbName.TabIndex = 9;
            this.tbName.Text = "NAME";
            this.tbName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblHP
            // 
            this.lblHP.AutoSize = true;
            this.lblHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP.Location = new System.Drawing.Point(13, 82);
            this.lblHP.Name = "lblHP";
            this.lblHP.Size = new System.Drawing.Size(35, 20);
            this.lblHP.TabIndex = 15;
            this.lblHP.Text = "HP:";
            // 
            // lblAttack
            // 
            this.lblAttack.AutoSize = true;
            this.lblAttack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack.Location = new System.Drawing.Point(13, 114);
            this.lblAttack.Name = "lblAttack";
            this.lblAttack.Size = new System.Drawing.Size(74, 20);
            this.lblAttack.TabIndex = 16;
            this.lblAttack.Text = "ATTACK:";
            // 
            // lblUnique
            // 
            this.lblUnique.AutoSize = true;
            this.lblUnique.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnique.Location = new System.Drawing.Point(13, 146);
            this.lblUnique.Name = "lblUnique";
            this.lblUnique.Size = new System.Drawing.Size(0, 20);
            this.lblUnique.TabIndex = 17;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(13, 50);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(59, 20);
            this.lblName.TabIndex = 18;
            this.lblName.Text = "NAME:";
            // 
            // UpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 225);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblUnique);
            this.Controls.Add(this.lblAttack);
            this.Controls.Add(this.lblHP);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tbUnique);
            this.Controls.Add(this.tbAttack);
            this.Controls.Add(this.tbHP);
            this.Controls.Add(this.cbClassSelect);
            this.Controls.Add(this.tbName);
            this.Name = "UpdateForm";
            this.Text = "UPDATE CHARACTER";
            this.Load += new System.EventHandler(this.UpdateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox tbUnique;
        private System.Windows.Forms.TextBox tbAttack;
        private System.Windows.Forms.TextBox tbHP;
        private System.Windows.Forms.ComboBox cbClassSelect;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblHP;
        private System.Windows.Forms.Label lblAttack;
        private System.Windows.Forms.Label lblUnique;
        private System.Windows.Forms.Label lblName;
    }
}